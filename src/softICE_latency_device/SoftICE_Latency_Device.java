package softICE_latency_device;


import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import org.ornet.cdm.AbstractIdentifiableContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.ActivateOperationDescriptor;
import org.ornet.cdm.AlertConditionPriority;
import org.ornet.cdm.AlertConditionType;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.AlertSignalManifestation;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.CauseInfo;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.CodedValue;
import org.ornet.cdm.ComponentActivation;
import org.ornet.cdm.ComponentState;
import org.ornet.cdm.ContextAssociationStateValue;
import org.ornet.cdm.CurrentAlertCondition;
import org.ornet.cdm.CurrentAlertSignal;
import org.ornet.cdm.CurrentAlertSystem;
import org.ornet.cdm.CurrentLimitAlertCondition;
import org.ornet.cdm.HydraMDSDescriptor;
import org.ornet.cdm.HydraMDSState;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.LimitAlertConditionDescriptor;
import org.ornet.cdm.LimitAlertObservationState;
import org.ornet.cdm.LocationContextDescriptor;
import org.ornet.cdm.LocationContextState;
import org.ornet.cdm.MDIB;
import org.ornet.cdm.MeasurementState;
import org.ornet.cdm.MetricAvailability;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.MetricMeasurementState;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericValue;
import org.ornet.cdm.PausableActivation;
import org.ornet.cdm.Range;
import org.ornet.cdm.RealTimeSampleArrayMetricDescriptor;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.RealTimeSampleArrayValue;
import org.ornet.cdm.RemedyInfo;
import org.ornet.cdm.SignalPresence;
import org.ornet.cdm.State;
import org.ornet.cdm.SystemContext;
import org.ornet.cdm.VMDDescriptor;
import org.ornet.softice.SoftICE;
import org.ornet.softice.provider.OSCPProvider;
import org.ornet.softice.provider.OSCPProviderActivateOperationHandler;
import org.ornet.softice.provider.OSCPProviderAlertConditionStateHandler;
import org.ornet.softice.provider.OSCPProviderMDStateHandler;
import org.ornet.softice.provider.OperationInvocationContext;
import org.ornet.softice.test.classes.DemoProviderFactory;
import org.yads.java.util.Log;

import shared.Shared;
import uro.latencyMeasurement.LatencyMeasurement;

public class SoftICE_Latency_Device {
	
	static class DemoNumericStateHandler extends OSCPProviderMDStateHandler<NumericMetricState> {      
       
        public DemoNumericStateHandler() {
            super(Shared.HANDLE_METRIC);
        }
        
        // Helper method
        private synchronized NumericMetricState createState(double value) {
            NumericMetricState nms = new NumericMetricState();
            nms.setState(ComponentActivation.ON);
            nms.setReferencedDescriptor(Shared.HANDLE_METRIC);
            NumericValue nv = new NumericValue();
            nv.setValue(BigDecimal.valueOf(value));
            MeasurementState msmntState = new MeasurementState();
            msmntState.setState(MetricMeasurementState.VALID);
            nv.setMeasurementState(msmntState);
            nms.setObservedValue(nv);
            return nms;
        }

        @Override
        public InvocationState onStateChangeRequest(NumericMetricState state, OperationInvocationContext oic) {
            System.out.println("Received numeric value change request: " + ((NumericMetricState)state).getObservedValue().getValue());
            return InvocationState.FINISHED;  // Request O.K., let SoftICE update internal MDIB
            //return InvocationState.CANCELLED;  // State will not be updated in internal MDIB
        }
        
        @Override
        protected NumericMetricState getInitialState() {
            return createState(1);
        }
        
        public final void setValue(double value) {
            NumericMetricState state = createState(value);
            // Update state in internal MDIB and notify consumers (MDIB version will be increased)
            updateState(state);
        }
    }
    
    static class AlwaysOnComponentStateHandler extends OSCPProviderMDStateHandler {

        private final boolean mds;
        
        public AlwaysOnComponentStateHandler(String descriptorHandle, boolean mds) {
            super(descriptorHandle);
            this.mds = mds;
        }

        @Override
        protected State getInitialState() {
            ComponentState state = mds? new HydraMDSState() : new ComponentState();
            state.setReferencedDescriptor(super.getDescriptorHandle());
            state.setState(ComponentActivation.ON);
            return state;
        }
        
    }

    public static synchronized OSCPProvider getDemoProvider(String epr) {
        OSCPProvider provider = new OSCPProvider();
        provider.setEndpointReference(epr);
        // Set some DPWS properties
        provider.setFriendlyName("SoftICE Latency Measurement Device");
        provider.setModelName("SoftICE Latency Measurement Device Model");

        HydraMDSDescriptor mds = new HydraMDSDescriptor();
        mds.setHandle("handle_mds");
        VMDDescriptor vmd = new VMDDescriptor();
        vmd.setHandle("handle_vmd");
        mds.getVMDs().add(vmd);     
        
        ChannelDescriptor chn = new ChannelDescriptor();
        chn.setHandle("handle_chn");
        vmd.getChannels().add(chn);
        

        //for some reason, a system context is required...
        LocationContextDescriptor lcd = new LocationContextDescriptor();
        lcd.setHandle("handle_context");
        SystemContext sc = new SystemContext();
        sc.setLocationContext(lcd);
        sc.setHandle("handle_sc");
        mds.setContext(sc);
        
        NumericMetricDescriptor nmd = new NumericMetricDescriptor();
        nmd.setCategory(MetricCategory.MEASUREMENT);
        nmd.setAvailability(MetricAvailability.CONTINUOUS);
        CodedValue unit = new CodedValue();
        unit.setCode("MDCX_DIMLESS");
        nmd.setUnit(unit);
        CodedValue type = new CodedValue();
        type.setCode("MDCX_METRIC");
        nmd.setType(type);
        nmd.setHandle(Shared.HANDLE_METRIC);
        nmd.setResolution(BigDecimal.ONE);
        chn.getMetrics().add(nmd);   

        
        provider.addHandler(new DemoNumericStateHandler());              

        // States for MDS, VMD and channel
        provider.addHandler(new AlwaysOnComponentStateHandler("handle_mds", true));   
        provider.addHandler(new AlwaysOnComponentStateHandler("handle_vmd", false));   
        provider.addHandler(new AlwaysOnComponentStateHandler("handle_chn", false));   

        
        provider.addHydraMDS(mds);

        return provider;
    }

    
    /**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length != 2){
			System.err.println("usage: java -D [...] -jar SoftICE_LatencyDevice <EPR_Postfix> <use schema validation 0/1>");
			System.exit(0);
		}
		
	
		String tmp = Shared.getInstance().getActualErp();
		Shared.getInstance().setActualErp(tmp+args[0]);
		
		boolean useSchemaValidation = true;
		
		int useSchemaValidationTmp = Integer.parseInt(args[1]);
		if(useSchemaValidationTmp == 0){
			useSchemaValidation = false;
		}
		
		SoftICE.getInstance().setSchemaValidationEnabled(useSchemaValidation);
		SoftICE.getInstance().startup();
		
		Log.setLogLevel(Log.DEBUG_LEVEL_ERROR);
        Log.setLogStackTrace(true);
        
        OSCPProvider provider = getDemoProvider(Shared.getInstance().getActualErp());
        provider.startup();      
        System.out.println("Provider running...");
		
		
		//some interaction
		String input = ""; //will contain the console input
		Scanner scanner = new Scanner(System.in); //we will use it to read from the console
		
		while(!input.equals("x")) { //"x" as about input
			System.out.println("Insert command. (end program with \"x\"; clear measurements with \"c\"; write data into files with \"w\")");
			
			//read console input
			input = scanner.nextLine();
			
			//abort criterion
			if (input.equals("x")) {
				break; //leave the loop
			}
			
			if (input.equals("w")) {
				LatencyMeasurement.getInstance().writeEveryMeasurementToFiles(true);
			}
			
			if (input.equals("c")) {
				LatencyMeasurement.getInstance().clearAllMeasurements();
			}

		}
		
		scanner.close();
		
	}
    
}