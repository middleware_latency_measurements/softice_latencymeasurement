package softICE_latency_client;

import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.OSCPConsumer;
import org.ornet.softice.consumer.OSCPServiceManager;
import org.yads.java.util.Log;

import shared.Shared;
import uro.latencyMeasurement.LatencyMeasurement;

public class SoftICE_Latency_Client {

	static OSCPConsumer consumer = null;
	
	

	public static void main(String[] args) {
		
		if(args.length != 5){
			System.err.println("usage: java -D [...] -jar SoftICE_LatencyDevice #numberOfMeasurements #delayBetweenMeasurements #delayBeforeMeasurements <EPR_Postfix> <use schema validation 0/1>");
			System.exit(0);
		}
		
		int numMeasurements = Integer.parseInt(args[0]);
		int delayBetweenMeasurements = Integer.parseInt(args[1]);
		int delayBeforeMeasurements = Integer.parseInt(args[2]);
		
		String tmp = Shared.getInstance().getActualErp();
		Shared.getInstance().setActualErp(tmp+args[3]);
		
		boolean useSchemaValidation = true;
		
		int useSchemaValidationTmp = Integer.parseInt(args[4]);
		if(useSchemaValidationTmp == 0){
			useSchemaValidation = false;
		}
		
		LatencyMeasurement.getInstance().setNumMeasurements(numMeasurements);
		LatencyMeasurement.getInstance().setDelayBetweenMeasurements(delayBetweenMeasurements);
		LatencyMeasurement.getInstance().setDelayBeforeMeasurements(delayBeforeMeasurements);

		
		if( !LatencyMeasurement.getInstance().useJamaicaVM ) {
		
			//framework initialization 
			SoftICE.getInstance().setSchemaValidationEnabled(useSchemaValidation);
			SoftICE.getInstance().setPortStart(30000); 
			SoftICE.getInstance().startup();             
	        
	        
	        
	        Log.setLogLevel(Log.DEBUG_LEVEL_ERROR);
	        Log.setLogStackTrace(true);
	        
	        //searching for one dedicated device with the given EPR
	        consumer = OSCPServiceManager.getInstance().discoverEPR(Shared.getInstance().getActualErp());
	        
	        
	        //start the search for the device again if nothings was found
	        while(consumer == null){ //null means nothing found
	        	System.err.println("Did NOT found the device :-( ");
	        	System.err.println("Trying again...");
	        	
	        	//retry
	        	consumer = OSCPServiceManager.getInstance().discoverEPR(Shared.getInstance().getActualErp());
	        }
	 
	        
	        //now we have found the device
	        System.err.println("Device found with EPR: " + consumer.getEndpointReference());

	        //create the thread that will trigger the requests for the measuremants
			Thread thread = new Thread(new RequestThread(consumer));
			thread.start();
	        
	
		}
	}
	
}
