package shared;

public class Shared {
	
	private static final String EPR = "EPR_SOFTICE_LATENCY_MEASUREMENT";
	public static final String HANDLE_METRIC = "handle_metric";
	
	private String actualErp = "";
	
	static Shared shared = null;
	
	private Shared() {
		// TODO Auto-generated constructor stub
	}
	
	public static Shared getInstance(){
		if(shared == null){
			shared = new Shared();
			
			shared.setActualErp(EPR);
		}
		
		return shared;
	}

	public String getActualErp() {
		return actualErp;
	}

	public void setActualErp(String actualErp) {
		this.actualErp = actualErp;
	}

}
